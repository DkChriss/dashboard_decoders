from django.db import models

# Create your models here.
class Decoder(models.Model):
    name = models.CharField(max_length=100)
    path = models.CharField(max_length=260)
    user = models.CharField(max_length=50)
    dev_eui = models.CharField(max_length=20)
    application_id = models.CharField(max_length=45)
    ip_server = models.GenericIPAddressField()
    state = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

class Payload(models.Model):
    payload_encoded_base64 = models.CharField(max_length=200)
    payload_decoded_base64 = models.CharField(max_length=200)
    payload_decoded = models.JSONField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)