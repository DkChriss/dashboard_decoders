from django import forms
from .models import Decoder
from .models import Payload
from django.forms import ModelForm

class DecoderForm(ModelForm):
    class Meta:
        model = Decoder
        fields = ['name','dev_eui', 'application_id', 'ip_server', 'state']


class PayloadForm(ModelForm):
    class Meta:
        model = Payload
        fields = ['payload_encoded_base64', 'payload_decoded_base64', 'payload_decoded']