import json
import base64
import paho.mqtt.client as mqtt
import requests
import decoder


data = {
    "payload_encoded" : "",
    "payload_decoded" : "",
    "payload": {}
}

#Connection
server = "EDITABLE"
application_id = "EDITABLE"
dev_eui = "EDITABLE"


def insert_data(data):
    data = {
        "payload_encoded_base64": data["payload_encoded"],
        "payload_decoded_base64": data["payload_decoded"],
        "payload_decoded": data["payload"]
    }
    response = requests.post('http://127.0.0.1:8000/api/post/', json=data)
    print(response)
    return response

def on_connect(client,userdata,flags,rc):
    print("Connected with result code: "+str(rc))
    suscribe = f"application/{application_id}/device/{dev_eui}/event/up"
    client.subscribe(suscribe)

def on_message(client, userdata, msg):
    payload = str(msg.payload.decode("utf-8"))    
    #JSON AND ENCODED PAYLODAD BASE64
    frame = json.loads(payload)
    data["payload_encoded"] = frame['data']
    #JSON AND DECODED PAYLOAD BASE64
    payload_decoded = base64.b64decode(frame['data']).hex() #DECODER PAYLOAD TO HEX FOR DECODING 
    data['payload_decoded'] = payload_decoded #SAVE PAYLOAD ON ATTRIBUTE
    #JSON AND DECODED PAYLOAD
    data["payload"] = decoder.decode(data["payload_decoded"])
    insert_data(data)
    return

mqttc= mqtt.Client()
mqttc.on_connect = on_connect
mqttc.on_message = on_message
mqttc.connect(server, 1883, 10)
mqttc.loop_forever()
