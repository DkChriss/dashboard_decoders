import json

#OBJECT
data = {
    "control_code": 0,
    "data_length": 0,
    "data_identification_1": 0,
    "data_identification_2": 0,
    "count_number": 0,
    "unity": "",
    "current_consumption": 0,
    "st_1": "",
    "st_2": "",
    "battery": ""
}

def decodeUnity(payload):
    hex_num = payload

    if hex_num == 0x2b:
        information = {
            "number": 0.001,
            "medition": "m³"
        }
    elif hex_num == 0x2c:
        information = {
            "number": 0.01,
            "medition": "m³"
        }
    elif hex_num == 0x2d:
        information = {
            "number": 0.1,
            "medition": "m³"
        }
    elif hex_num == 0x2e:
        information = {
            "number": 1,
            "medition": "m³"
        }
    elif hex_num == 0x35:
        information = {
            "number": 0.0001,
            "medition": "m³/h"
        }
    else:
        information = {
            "status" : "Desconocido"
        }

    return information


def decodeCurrentConsumption(payload, unity):
    hex_num = payload
    hex_reverse = "".join(map(str.__add__,hex_num[-2::-2],hex_num[-1::-2]))
    consumption = int(hex_reverse) * unity['number']
    return consumption

def decodeST1(bcd_num):
    reserved_1 = "Normal" if int(bcd_num[0]) == 0 else "Error"
    reserved_2 = "Normal" if int(bcd_num[1]) == 0 else "Error"
    reserved_3 = "Normal" if int(bcd_num[2]) == 0 else "Error"
    reserved_4 = "Normal" if int(bcd_num[3]) == 0 else "Error"
    reserved_5 = "Normal" if int(bcd_num[4]) == 0 else "Error"
    reserved_6 = "Normal" if int(bcd_num[5]) == 0 else "Error"
    reserved_7 = "Normal" if int(bcd_num[6]) == 0 else "Error"
    reserved_8 = "Normal" if int(bcd_num[7]) == 0 else "Error"
    st_1_alarms = {
        "reserved": reserved_1,
        "reserved_2": reserved_2,
        "reserved_3": reserved_3,
        "reserved_4": reserved_4,
        "reserved_5": reserved_5,
        "reserved_6": reserved_6,
        "reserved_7": reserved_7,
        "reserved_8": reserved_8
    }
    return st_1_alarms

def decodeST2(bcd_num):
    battery =           "Normal" if int(bcd_num[0]) == 0 else "Error"
    emptyPipe =         "Normal" if int(bcd_num[1]) == 0 else "Error"
    reverseFlow =       "Normal" if int(bcd_num[2]) == 0 else "Error"
    overRange =         "Normal" if int(bcd_num[3]) == 0 else "Error"
    overTemperature =   "Normal" if int(bcd_num[4]) == 0 else "Error"
    eeprom =            "Normal" if int(bcd_num[5]) == 0 else "Error"
    reserved =          "Normal" if int(bcd_num[6]) == 0 else "Error"
    reserved_1 =        "Normal" if int(bcd_num[7]) == 0 else "Error"
    alarms = {
        "LowBatteryAlarm": battery,
        "emptyPipeAlarm": emptyPipe,
        "reverseFlowAlarm": reverseFlow,
        "overRangeAlarm": overRange,
        "overRangeTemperature": overTemperature,
        "EEPROM_ERROR": eeprom,
        "reserved": reserved,
        "reserved_2": reserved_1
    }

    return alarms

def decodeBattery(payload):
    hex_num = int(payload,16)
    battery = (hex_num-1)/253*100
    return battery

def convertHexToBCD(payload):
    hex_num = payload
    dec_num = int(hex_num, 16)

    bin_num = ""
    if(dec_num == 0 ):
        bin_num = "00000000"      
    else:
        while dec_num > 0:
            digit = dec_num % 10
            bcd_digit = bin(digit)[2:].zfill(4)
            bin_num = bcd_digit + bin_num
            dec_num //= 10

    return bin_num

def convertBCDtoDecimal(bcd_number):
    bin_num = bin_num
    dec_num = 0
    mul = 1

    while len(bin_num) > 0:
        bcd_digit = bin_num[-4:]
        dec_num += (int(bcd_digit, 2) * mul)
        mul *= 10
        bin_num = bin_num[:-4]

    return dec_num

def decode (payload):
    
    #Control Codde
    control_code = int(payload[0:2],base=16)
    data["control_code"] = control_code
    
    #Data Length
    data_length = int(payload[2:4],base=16)
    data["data_length"] = data_length
    
    #Data idenditifaction-1
    data_identification_1 = int(payload[4:6],base=16)
    data["data_identification_1"] = data_identification_1
    
    #Data identification-2
    data_identification_2 = int(payload[6:8],base=16)
    data["data_identification_2"] = data_identification_2
    
    #Count Number
    count_number = int(payload[8:10],base=16)
    data["count_number"] = count_number
    
    #Unity
    unity = int(payload[10:12],16)   
    data["unity"] = decodeUnity(unity)
    
    #Current consumption
    current_consumption = payload[12:20]
    data["current_consumption"] = decodeCurrentConsumption(current_consumption,data["unity"])
    
    #ST1
    st_1 = payload[20:22]
    st_1_bcd = convertHexToBCD(st_1)
    data["st_1"] = decodeST1(st_1_bcd)
    
    #ST2
    st_2 = payload[22:24]
    st_2_bcd = convertHexToBCD(st_2)
    data["st_2"] = decodeST2(st_2_bcd)
    
    #BATtERY
    battery = payload[24:26]
    data["battery"] = decodeBattery(battery)
    
    return data
