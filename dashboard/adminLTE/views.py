from django.shortcuts import render
#MODELS
from .models import Decoder
from .models import Payload
#FORMS
from .form import DecoderForm
from .form import PayloadForm
#RESPONSES
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseNotFound, JsonResponse
from django.forms.models import model_to_dict
#FILES
import shutil
import os
from os import path

# Create your views here.
#FILES
def runFile(name):
    os.system(f'{name}_decoder.py')

def getPath(name):
    filename = f'{name}_decoder.py'
    current_dir = os.getcwd()  # get current working directory

    for root, dirs, files in os.walk(current_dir):
        if filename in files:
            file_path = os.path.join(root, filename)
            return file_path
    else:
        print(f"Could not find {filename} in {current_dir}")

def copyFile(name):
    shutil.copy('adminLTE/python/decoder.py', f'adminLTE/python/{name}_decoder.py')

def modifyFile(name,server,application_id,dev_eui):
    if(os.path.exists(f'adminLTE/python/{name}_decoder.py')):
        fileDecoder = open(f'adminLTE/python/{name}_decoder.py', 'r')
        data = fileDecoder.readlines()
        data[13] = f'server = "{server}"\n'
        data[14] = f'application_id = "{application_id}"\n'
        data[15] = f'dev_eui = "{dev_eui}"\n'
        fileDecoder = open(f'adminLTE/python/{name}_decoder.py', 'w')
        fileDecoder.writelines(data)
        fileDecoder.close()

def changeName(oldName, newName):
    if path.exists(f'adminLTE/python/{oldName}_decoder.py'):
        src = path.realpath(f'adminLTE/python/{oldName}_decoder.py')
        os.rename(f'adminLTE/python/{oldName}_decoder.py', f'adminLTE/python/{newName}_decoder.py')

def index(request):
    return render(request, 'adminlte/index.html')

def show(request):
    return render(request, 'adminlte/list_decoders.html')

def list_decoder(_request):
    decoders = list(Decoder.objects.values())
    response = {
        'data': decoders
    }
    return JsonResponse(response)

def get_decoder(request, id):
    if request.method == 'GET':
        try:
            response = model_to_dict(Decoder.objects.get(id = id))
            return JsonResponse({
                'error': False, 
                'data': response
            })
        except Payload.DoesNotExist:
            return JsonResponse({'status': 'Fail', 'msg': 'El objeto seleccionado no existe'})

def store(request):
    if request.method == 'POST':
        try:
            name = request.POST.get('name')
            dev_eui = request.POST.get('dev_eui')
            application_id = request.POST.get('application_id')
            ip_server = request.POST.get('ip_server')
            state = request.POST.get('state')
            user = 'USER'
            if(state == 'on'):
                state = True
            else:
                state = False
            copyFile(name)
            path = getPath(name)
            #path = 'PATH'
            modifyFile(name, ip_server, application_id, dev_eui)
            Decoder.objects.create(
                name = name,
                path = path,
                user = user,
                dev_eui = dev_eui,
                application_id = application_id,
                ip_server = ip_server,
                state = state)
            message = 'REGISTRADO CORRECTAMENTE'
            error = 'No existe un error'
            response = JsonResponse({
                'message': message,
                'error': error
            })
            response.status_code = 201
            return response
        except ValueError:
            message = 'Ocurrio Error'
            error = ValueError
            response = JsonResponse({
                'message': message,
                'error': error
            })
            response.status_code = 400
            return response


def update(request,id):
    if request.method == 'POST':
        try:
            name = request.POST.get('name')
            user = 'USER'
            dev_eui = request.POST.get('dev_eui')
            application_id = request.POST.get('application_id')
            ip_server = request.POST.get('ip_server')
            state = request.POST.get('state')
            if(state == 'on'):
                state = True
            else:
                state = False 
            decoder = Decoder.objects.get(id=id)
            #path = 'PATH'
            changeName(decoder.name, name)
            path = getPath(name)
            modifyFile(name, ip_server, application_id,dev_eui)
            decoder.name = name
            decoder.path = path
            decoder.user = user
            decoder.dev_eui = dev_eui
            decoder.application_id = application_id
            decoder.ip_server = ip_server
            decoder.state = state
            decoder.save()
            message = 'Se actualizo correctamente'
            error = 'No existe un error'
            response = JsonResponse({
                'message': message,
                'error': error
            })
            response.status_code = 201
            return response
        except ValueError:
            message = 'Ocurrio Error'
            error = ValueError
            response = JsonResponse({
                'message': message,
                'error': error
            })
            response.status_code = 400
            return response
        
def destroy(request,id):
    if request.method == 'POST':
        try:
            response = Decoder.objects.get(id = id)
            if(os.path.exists(f'adminLTE/python/{response.name}_decoder.py')):
                os.remove(f'adminLTE/python/{response.name}_decoder.py')
            response.delete()
            return JsonResponse({
                'error': False, 
                'message': 'Se elimino correctamente'
            })
        except Payload.DoesNotExist:
            return JsonResponse({'status': 'Fail', 'msg': 'El objeto seleccionado no existe'})