from django.urls import path
from . import views

urlpatterns = [ 
    path('', views.index, name='index'),
    path('lista-decodificadores', views.show, name='show'),
    path('decoder/store', views.store, name='store'),
    path('list_decoder', views.list_decoder, name='list'),
    path('get_decoder/<int:id>/', views.get_decoder, name='get'),
    path('decoder/update/<int:id>/', views.update, name='update'),
    path('delete_decoder/<int:id>/', views.destroy, name='delete')
]