from rest_framework.serializers import ModelSerializer
from adminLTE.models import Payload

class PayloadSerializer(ModelSerializer):
    class Meta:
        model = Payload
        fields = ['payload_encoded_base64', 'payload_decoded_base64', 'payload_decoded']