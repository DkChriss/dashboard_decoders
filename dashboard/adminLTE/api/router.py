from rest_framework.routers import DefaultRouter
from adminLTE.api.views import PayloadApiViewSet

router_payload = DefaultRouter()

router_payload.register(prefix='post', basename='post', viewset=PayloadApiViewSet)