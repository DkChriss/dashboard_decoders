from rest_framework.viewsets import ModelViewSet
from adminLTE.models import Payload
from adminLTE.api.serializers import PayloadSerializer

class PayloadApiViewSet(ModelViewSet):
    serializer_class = PayloadSerializer
    queryset = Payload.objects.all()